from fractions import *

def answer(m):
    # convert nums into fractions
    print("m")
    print(m)
    m = fractionalize(m)
    print("m frac")
    print(m)
    
    # determine transient and absorbing states
    transient = []
    absorbing = []

    for i in range(len(m)):
        # if it has a nonzero in it, it's a transient state, otherwise
        # absorbing
        isTransient = False
        for j in range(len(m[i])):
            if m[i][j] != 0:
                transient.append(i)
                isTransient = True
                break
        if not isTransient: absorbing.append(i)

    print("transient: " + str(transient))
    print("absorbing: " + str(absorbing))

    # make the Q and R matrices
    q = makeQ(m, transient)
    r = makeR(m, transient, absorbing)

    print("Q")
    print(q)
    print("r")
    print(r)

    n = calcN(m, q)

    print("n")
    print(n)
    
    b = calcB(n, r)
    print("b")
    print(b)

    answerRow = defractionalize(b[0])
    print("answers:")
    print(answerRow)
    return(answerRow)

def defractionalize(row):
    #maxDenom = max([x.denominator for x in row])
    #print("maxDenom: " + str(maxDenom))

    numerators = [x.numerator for x in row]
    denominators = [x.denominator for x in row]


    settled = False
    while not settled:
        for i in range(len(denominators) - 1):
            cd = gcd(denominators[i], denominators[i+1])
            multiple1 = denominators[i] / cd
            multiple2 = denominators[i+1] / cd

            numerators[i] *= multiple2
            denominators[i] *= multiple2
            
            numerators[i+1] *= multiple1
            denominators[i+1] *= multiple1

            if multiple1 == 1 and multiple2 == 1: settled = True
        print(numerators)
        print(denominators)

    answer = numerators
    answer.append(denominators[0])
    return answer

def fractionalize(m):
    m_ = []

    for i in range(len(m)):
        rowSum = 0
        for j in range(len(m[i])):
            rowSum += m[i][j]

        if rowSum == 0: rowSum = 1 # no zeros in denom

        m_.append([])
        for j in range(len(m[i])):
            m_[i].append(Fraction(m[i][j], rowSum))

    return m_
    
# Q = p(transient states x transient states)
def makeQ(m, transient):
    q = []

    for i in transient:
        q.append([])
        for j in transient:
            q[-1].append(m[i][j])

    return q

# R = p(transient states x absorbing states)
def makeR(m, transient, absorbing):
    r = []
    
    for i in transient:
        r.append([])
        for j in absorbing:
            r[-1].append(m[i][j])

    return r

def calcN(m, q):
    # copy everything into n
    n = []
    for i in range(len(q)):
        n.append([])
        for j in range(len(q[i])):
            n[i].append(q[i][j])

    # first subtract Q from identity matrix
    for i in range(len(q)):
        for j in range(len(q[i])):
            print(str(i) + "," + str(j))
            if i == j:
                n[i][j] = 1 - n[i][j]
            else:
                n[i][j] = 0 - n[i][j]

    # invert matrix
    n = m_invert(n)
    return n

def calcB(n, r):
    print("MULTIPLYING:")
    print(n)
    print(r)
    b = m_multiply(n, r)
    print("RESULT:")
    print(b)
    print("---")
    return b
    
def m_multiply(m1, m2):
    m3 = []

    rows = len(m1)
    cols = len(m2[0])

    for row in range(rows):
        m3.append([])
        for col in range(cols):
            print(str(row + 1) + "," + str(col + 1))
            result = 0
            for i in range(len(m1[0])):
                for j in range(len(m2)):
                    if i == j:
                        result += m1[row][i]*m2[j][col]
                        print(str(m1[row][i]) + " * " + str(m2[j][col]))
            print(result)
            m3[row].append(result)

    return m3

def m_invert(m):
    # add identity matrix
    dim = len(m)

    print("m invert init")
    print(m)
    
    for i in range(dim):
        for j in range(dim):
            if i == j: m[i].append(Fraction(1,1))
            else: m[i].append(Fraction(0,1))
            
    print("m invert append")
    print(m)
    
    # put left side in REF
    for k in range(len(m)):
        print("k: " + str(k))
        # find max kth pos
        maxIndex = 0
        maxVal = m[0][k]

        for i in range(len(m)):
            if abs(m[i][k]) > maxVal:
                maxIndex = i
                maxVal = m[i][k]

        m_swapRows(m, k, maxIndex)

        for i in range(k+1,len(m)):
            f = m[i][k] / m[k][k]
            print("f")
            print(f)
            m_addRowScalarMultiple(m, i, k, -f)
             
            m[i][k] = Fraction(0,1)

        # if doesn't start with 1, fix
        if m[k][k] != Fraction(1,1):
            # invert fraction
            f = Fraction(m[k][k].denominator, m[k][k].numerator)
            m_rowScalarMul(m, k, f)

    print("m finished ref")
    print(m)

    # put left side in RREF
    for row in range(len(m)):
        for col in range(row+1, len(m)):
            if m[row][col] != Fraction(0,1):
                f = m[row][col]
                m_addRowScalarMultiple(m, row, col, -f)

    print("m finished rref")
    print(m)

    # return only the right side
    m_ = []
    for i in range(len(m)):
        m_.append( m[i][dim:])

    print("m_")
    print(m_)
        
    return m_

def m_swapRows(m, r1, r2):
    #print("Swapping " + str(r1) + " with " + str(r2))
    #print("m swap init")
    #print(m)
    
    #temp = m[r2]
    temp = [x for x in m[r2]]
    #m[r2] = m[r1]
    m[r2] = [x for x in m[r1]]
    m[r1] = temp
    
    #print("m swap finit")
    #print(m)

def m_rowScalarMul(m, r1, s):
    for i in range(len(m[r1])):
        m[r1][i] = m[r1][i]*s

def m_addRowScalarMultiple(m, r1, r2, s):
    for i in range(len(m[r1])):
        m[r1][i] = m[r1][i] + s*m[r2][i]

#def op_calc(v);
    #return float(v[0]) / float(v[1])
#
#def op_add(v1, v2):
    #return 
