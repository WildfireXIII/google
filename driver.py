from solution import *

input1 = [[0,2,1,0,0],[0,0,0,3,4],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
out1 = [7,6,8,21]

test1 = answer(input1)
print(test1)
print(out1)
if test1 != out1:
    print("Test 1 failed")

input2 = [
        [0,1,0,0,0,1],
        [4,0,0,3,2,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0],
        [0,0,0,0,0,0]]
out2 = [0,3,2,9,14]

test2 = answer(input2)
print(test2)
print(out2)
if test2 != out2:
    print("Test 2 failed")
    
input3 = [
        [0,0,0],
        [0,0,0],
        [0,0,0]]
out3 = [1,0,0,1]

test3 = answer(input3)
print(test3)
print(out3)
if test3 != out3:
    print("Test 3 failed")
