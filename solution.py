from fractions import *

def answer(m):
    # convert nums into fractions
    m = fractionalize(m)
    
    # determine transient and absorbing states
    transient = []
    absorbing = []

    for i in range(len(m)):
        # if it has a nonzero in it, it's a transient state, otherwise
        # absorbing
        isTransient = False
        for j in range(len(m[i])):
            if m[i][j] != 0:
                transient.append(i)
                isTransient = True
                break
        if not isTransient: absorbing.append(i)

    # we can stop at the first state
    if 0 not in transient:
        answerRow = [1]
        for i in range(len(absorbing) - 1):
            answerRow.append(0)
        answerRow.append(1)
        return answerRow

    # make the Q and R matrices
    q = makeQ(m, transient)
    r = makeR(m, transient, absorbing)

    n = calcN(m, q)
    b = calcB(n, r)

    answerRow = defractionalize(b[0])
    return(answerRow)

def defractionalize(row):
    numerators = [x.numerator for x in row]
    denominators = [x.denominator for x in row]

    # gcd until all same denominator
    settled = False
    while not settled:
        localSettled = True
        for i in range(len(denominators) - 1):
            cd = gcd(denominators[i], denominators[i+1])
            multiple1 = denominators[i] / cd
            multiple2 = denominators[i+1] / cd

            numerators[i] *= multiple2
            denominators[i] *= multiple2
            
            numerators[i+1] *= multiple1
            denominators[i+1] *= multiple1

            if multiple1 != 1 or multiple2 != 1: localSettled = False
        if localSettled: settled = True

    equal = True
    denom = denominators[0]
    for i in range(len(denominators)):
        if denominators[i] != denom: equal = False
    assert(equal)

    answer = numerators
    answer.append(denominators[0])
    return answer

def fractionalize(m):
    m_ = []

    for i in range(len(m)):
        rowSum = 0
        for j in range(len(m[i])):
            rowSum += m[i][j]

        if rowSum == 0: rowSum = 1 # no zeros in denom

        m_.append([])
        for j in range(len(m[i])):
            m_[i].append(Fraction(m[i][j], rowSum))

    return m_
    
# Q = p(transient states x transient states)
def makeQ(m, transient):
    q = []

    for i in transient:
        q.append([])
        for j in transient:
            q[-1].append(m[i][j])

    return q

# R = p(transient states x absorbing states)
def makeR(m, transient, absorbing):
    r = []

    assert(len(transient) > 0)
    assert(len(absorbing) > 0)
    
    for i in transient:
        r.append([])
        for j in absorbing:
            r[-1].append(m[i][j])

    return r

def calcN(m, q):
    # copy everything into n
    n = []
    for i in range(len(q)):
        n.append([])
        for j in range(len(q[i])):
            n[i].append(q[i][j])

    # first subtract Q from identity matrix
    for i in range(len(q)):
        for j in range(len(q[i])):
            if i == j:
                n[i][j] = 1 - n[i][j]
            else:
                n[i][j] = 0 - n[i][j]

    # invert matrix
    n = m_invert(n)
    return n

def calcB(n, r):
    b = m_multiply(n, r)
    return b
    
def m_multiply(m1, m2):
    m3 = []

    rows = len(m1)
    cols = len(m2[0])

    for row in range(rows):
        m3.append([])
        for col in range(cols):
            result = 0
            for i in range(len(m1[0])):
                for j in range(len(m2)):
                    if i == j:
                        result += m1[row][i]*m2[j][col]
            m3[row].append(result)

    return m3

def m_invert(m):
    # add identity matrix
    dim = len(m)

    for i in range(dim):
        for j in range(dim):
            if i == j: m[i].append(Fraction(1,1))
            else: m[i].append(Fraction(0,1))
    
    # put left side in REF
    for k in range(len(m)):
        # find max kth pos
        maxIndex = 0
        maxVal = m[0][k]

        for i in range(len(m)):
            if abs(m[i][k]) > maxVal:
                maxIndex = i
                maxVal = m[i][k]

        m_swapRows(m, k, maxIndex)

        for i in range(k+1,len(m)):
            f = m[i][k] / m[k][k]
            #print("f")
            #print(f)
            m_addRowScalarMultiple(m, i, k, -f)
             
            m[i][k] = Fraction(0,1)

        # if doesn't start with 1, fix
        if m[k][k] != Fraction(1,1):
            # invert fraction
            f = Fraction(m[k][k].denominator, m[k][k].numerator)
            m_rowScalarMul(m, k, f)

    # put left side in RREF
    for row in range(len(m)):
        for col in range(row+1, len(m)):
            if m[row][col] != Fraction(0,1):
                f = m[row][col]
                m_addRowScalarMultiple(m, row, col, -f)

    # return only the right side
    m_ = []
    for i in range(len(m)):
        m_.append( m[i][dim:])

    return m_

def m_swapRows(m, r1, r2):
    temp = [x for x in m[r2]]
    m[r2] = [x for x in m[r1]]
    m[r1] = temp

def m_rowScalarMul(m, r1, s):
    for i in range(len(m[r1])):
        m[r1][i] = m[r1][i]*s

def m_addRowScalarMultiple(m, r1, r2, s):
    for i in range(len(m[r1])):
        m[r1][i] = m[r1][i] + s*m[r2][i]
